<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class NewStudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = $request->input('student_id');

        $s_email = $request->input('semail');
        $shn = $request->input('shn');
        $s_street = $request->input('s-street');
        $s_brgy = $request->input('sbrgy');
        $s_sub_build = $request->input('sbuild');
        $s_district = $request->input('sdistrict');
        $s_city= $request->input('scity');
        $s_prov = $request->input('sprov');
        $s_pcode = $request->input('spcode');
        $s_region = $request->input('sregion');

        $m_email = $request->input('memail');
        $mhn = $request->input('mhn');
        $m_street = $request->input('m-street');
        $m_brgy = $request->input('mbrgy');
        $m_sub_build = $request->input('mbuild');
        $m_district = $request->input('mdistrict');
        $m_city= $request->input('mcity');
        $m_prov = $request->input('mprov');
        $m_pcode = $request->input('mpcode');
        $m_region = $request->input('mregion');

        $f_email = $request->input('femail');
        $fhn = $request->input('fhn');
        $f_street = $request->input('f-street');
        $f_brgy = $request->input('fbrgy');
        $f_sub_build = $request->input('fbuild');
        $f_district = $request->input('fdistrict');
        $f_city= $request->input('fcity');
        $f_prov = $request->input('fprov');
        $f_pcode = $request->input('fpcode');
        $f_region = $request->input('fregion');

        $s_landline = $request->input('slandline');
        $s_cpnumber = $request->input('scpnumber');

        $m_landline = $request->input('mlandline');
        $m_cpnumber = $request->input('mcpnumber');

        $f_landline = $request->input('flandline');
        $f_cpnumber = $request->input('fcpnumber');

        if(!(empty($s_landline))) {
            DB::table('contacts')->updateOrInsert(['student_id' => $user_id, 'table_name'=> 'contacts', 'relationship' => 3, 'contact_type' => 1, 'change_contact' => ""], ['contact' => $s_landline]);
        }

        if(!(empty($s_cpnumber))) {
            DB::table('contacts')->updateOrInsert(['student_id' => $user_id, 'table_name'=> 'contacts', 'relationship' => 3, 'contact_type' => 2,'change_contact' => ""], ['contact' => $s_cpnumber]);
        }

        if(!(empty($m_landline))) {
            DB::table('contacts')->updateOrInsert(['student_id' => $user_id, 'table_name'=> 'contacts', 'relationship' => 1, 'contact_type' => 1,'change_contact' => ""], ['contact' => $m_landline]);
        }

        if(!(empty($m_cpnumber))) {
            DB::table('contacts')->updateOrInsert(['student_id' => $user_id, 'table_name'=> 'contacts', 'relationship' => 1, 'contact_type' => 2,'change_contact' => ""], ['contact' => $m_cpnumber]);
        }

        if(!(empty($f_landline))) {
            DB::table('contacts')->updateOrInsert(['student_id' => $user_id, 'table_name'=> 'contacts', 'relationship' => 1, 'contact_type' => 1, 'change_contact' => ""], ['contact' => $f_landline]);
        }

        if(!(empty($f_cpnumber))) {
            DB::table('contacts')->updateOrInsert(['student_id' => $user_id, 'table_name'=> 'contacts', 'relationship' => 1, 'contact_type' => 2,'change_contact' => ""], ['contact' => $f_cpnumber]);
        }

        DB::table('student_data')->where('student_id', $request->student_id)->update(['last_edited' =>  \Carbon\Carbon::now()]);

        if(!(empty($s_email))){
             DB::table('student_personal_data')->where('student_id', $user_id)->update(['xavier_email' => $s_email]);
        };

        if(!(empty($shn))){
        DB::table('students_address')->where('student_id', $user_id)->update(['house_number' => $shn]);
         };

        if(!(empty($s_street))){
            DB::table('students_address')->where('student_id', $user_id)->update(['street' => $s_street]);
        };

        if(!(empty($s_brgy))){
            DB::table('students_address')->where('student_id', $user_id)->update(['barangay' => $s_brgy]);
        };

        if(!(empty($s_sub_build))){
            DB::table('students_address')->where('student_id', $user_id)->update(['subdivision_building' => $s_sub_build]);
        };

        if(!(empty($s_district))){
            DB::table('students_address')->where('student_id', $user_id)->update(['district' => $s_district]);
        };

        if(!(empty($s_pcode))){
            DB::table('students_address')->where('student_id', $user_id)->update(['postal_code' => $s_pcode]);
        };

        if(!(empty($s_pcode))){
            DB::table('students_address')->where('student_id', $user_id)->update(['postal_code' => $s_pcode]);
        };

        if(!(empty($s_city))){
            DB::table('students_address')->where('student_id', $user_id)->update(['city' => $s_city]);
        };

        if(!(empty($s_prov))){
            DB::table('students_address')->where('student_id', $user_id)->update(['province_municipality' => $s_prov]);
        };

        if(!(empty($s_region))){
            DB::table('students_address')->where('student_id', $user_id)->update(['region' => $s_region]);
        };

        if(!(empty($s_mcontact))){
            DB::table('contacts')->where(['student_id', $user_id],['relationship', '=', 1])->update(['contact' => $s_mcontact]);
        };

        if(!(empty($s_fcontact))){
            DB::table('contacts')->where(['student_id', $user_id],['relationship', '=', 2])->update(['contact' => $s_fcontact]);
        };

        if(!(empty($s_email))){
            DB::table('student_personal_data')->where('student_id', $user_id)->update(['xavier_email' => $s_email]);
        };

        if(!(empty($mhn))){
            DB::table('mother_address')->where('student_id', $user_id)->update(['house_number' => $mhn]);
        };

        if(!(empty($m_street))){
            DB::table('mother_address')->where('student_id', $user_id)->update(['street' => $m_street]);
        };

        if(!(empty($m_brgy))){
            DB::table('mother_address')->where('student_id', $user_id)->update(['barangay' => $m_brgy]);
        };

        if(!(empty($m_sub_build))){
            DB::table('mother_address')->where('student_id', $user_id)->update(['subdivision_building' => $m_sub_build]);
        };

        if(!(empty($m_district))){
            DB::table('mother_address')->where('student_id', $user_id)->update(['district' => $m_district]);
        };

        if(!(empty($m_pcode))){
            DB::table('mother_address')->where('student_id', $user_id)->update(['postal_code' => $m_pcode]);
        };

        if(!(empty($m_city))){
            DB::table('mother_address')->where('student_id', $user_id)->update(['city' => $m_city]);
        };

        if(!(empty($m_prov))){
            DB::table('mother_address')->where('student_id', $user_id)->update(['province_municipality' => $m_prov]);
        };

        if(!(empty($m_region))){
            DB::table('mother_address')->where('student_id', $user_id)->update(['region' => $m_region]);
        };

        if(!(empty($m_email))){
            DB::table('mother_personal_data')->where('student_id', $user_id)->update(['email' => $m_email]);
        };

        if(!(empty($fhn))){
            DB::table('father_address')->where('student_id', $user_id)->update(['house_number' => $fhn]);
        };

        if(!(empty($f_street))){
            DB::table('father_address')->where('student_id', $user_id)->update(['street' => $f_street]);
        };

        if(!(empty($f_brgy))){
            DB::table('father_address')->where('student_id', $user_id)->update(['barangay' => $f_brgy]);
        };

        if(!(empty($f_sub_build))){
            DB::table('father_address')->where('student_id', $user_id)->update(['subdivision_building' => $f_sub_build]);
        };

        if(!(empty($f_district))){
            DB::table('father_address')->where('student_id', $user_id)->update(['district' => $f_district]);
        };

        if(!(empty($f_pcode))){
            DB::table('father_address')->where('student_id', $user_id)->update(['postal_code' => $f_pcode]);
        };

        if(!(empty($f_city))){
            DB::table('father_address')->where('student_id', $user_id)->update(['city' => $f_city]);
        };

        if(!(empty($f_prov))){
            DB::table('father_address')->where('student_id', $user_id)->update(['province_municipality' => $f_prov]);
        };

        if(!(empty($f_region))){
            DB::table('father_address')->where('student_id', $user_id)->update(['region' => $f_region]);
        };

        if(!(empty($f_email))){
            DB::table('father_personal_data')->where('student_id', $user_id)->update(['email' => $f_email]);
        };

        return redirect('dashboard')->with('success', 'Response has been submitted');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
