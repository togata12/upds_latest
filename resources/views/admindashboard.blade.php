@extends('layouts.adminapp')
@section('title', 'Dashboard')
@section('admin-content')
    <div class="page-content-wrapper">
        <div class="card info shadow my-3">
            <div class="card-header-info">
                <div class="d-flex ">
                    <img src="{{asset('img/XS_seal_update.png')}}" height="75" alt="Logo">
                    <h2 class="header-xs">Xavier School</h2>
                    <span class="header-xs-span-3">Administrator Panel</span>
                    <span class="header-xs-span">W.Conservation Avenue, Nuvali, Calamba Laguna</span>
                    <span class="header-xs-span-2">Telephone Numbers: (02) 664-0143 (049) 576-2560 (02) 542-2646</span>
                </div>
                <hr>
            </div>
            <div class="card-body">
                <div class="d-flex justify-content-between">
                    <div>
                        @include('inc.searchbar')
                    </div>
                    <div>
                        {{ $forapprovals->links() }}
                    </div>
                </div>
                <div class="table-responsive shadow my-2">
                    <table class="table table-striped table-hover">
                        <thead style="background:#03104e;" class="text-white">
                            <tr>
                                <th class="text-center" colspan="7"><h5>The Data Below is the Requested Data for Changes</h5></th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Student ID</th>
                                <th>Student Name</th>
                                <th>Requested Changes</th>
                                <th>Changes To</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                            <tbody class="table-hover" id="myTable">
                                @if(count($forapprovals) > 0 )
                                    @foreach($forapprovals as $approval)
                                        <tr>
                                            <td>{{ $approval->id }}</td>
                                            <td>{{ $approval->student_id }}</td>
                                            <td>{{ $approval->Name }}</td>
                                            <td class="text-primary">{{ $approval-> request_for_approval }}</td>
                                            <td>{{ $approval-> relationship }}</td>
                                            @if($approval->status == "PENDING")
                                                <td class="text-pending"><i class="fa fa-exclamation-triangle"></i> {{$approval->status}}</td>
                                            @elseif($approval->status == "ACCEPTED")
                                                <td class="text-success"><i class="fa fa-check-circle"></i> {{$approval->status}}</td>
                                            @else
                                                <td class="text-danger"><i class="fa fa-times-circle"></i> {{$approval->status}}</td>
                                            @endif
                                            <td>
                                                @if($approval -> status == "PENDING")
                                                    <div class="d-flex">
                                                        <div class="accept-button buttons mr-2">
                                                            <form action="{{ url('/updateinfos/'. $approval->id) }}" method="POST">
                                                                @csrf
                                                                @method('PUT')
                                                                <input type="hidden" name="student_id" value="{{ $approval->student_id }}">
                                                                <input type="hidden" name="table_name" value="{{ $approval->table_name }}">
                                                                <input type="hidden" name="Name" value="{{ $approval->Name }}">
                                                                <input type="hidden" name="relationship" value="{{ $approval-> relationship }}">
                                                                <input type="hidden" name="requestapproval" value="{{ $approval-> request_for_approval }}">
                                                                <input type="hidden" name="column" value="{{ $approval->type_of_column}}">
                                                                <input type="hidden" name="contact_type" value="{{ $approval->contact_type}}">
                                                                <input type="hidden" name="relationshipcontact" value="{{ $approval->contact_relationship}}">
                                                                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-check"></i> ACCEPT</button>
                                                            </form>
                                                        </div>
                                                        <div class="reject-button buttons">
                                                            <form action="{{ url('/rejectstatus/'. $approval->id) }}" method="POST">
                                                                @csrf
                                                                @method('PUT')
                                                                <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> REJECT</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                @endif
                                            </td>
                                        </tr> 
                                    @endforeach
                            </tbody>
                    </table>
                </div>
                <div class="links">
                    {{ $forapprovals->links() }}
                </div>
                @else
                    <div class="no-data">
                        <h1> No Data to Be Displayed </h1>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
