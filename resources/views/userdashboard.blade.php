@extends('layouts.app')

@section('title', 'Home')

@section('existinguser')
   <div class="page-content-wrapper">
       @if($studentStatus != true && $motherStatus !=  true && $fatherStatus != true)
           <form action="{{ route('store.info') }}" method="POST">
               @csrf
               <div class="card info shadow my-3">
                   <div class="card-header-info">
                       <div class="d-flex ">
                           <img src="{{asset('img/XS_seal_update.png')}}" height="75" alt="Logo">
                           <h2 class="header-xs">Xavier School</h2>
                           <span class="header-xs-span">W.Conservation Avenue, Nuvali, Calamba Laguna</span>
                           <span class="header-xs-span-2">Telephone Numbers: (02) 664-0143 (049) 576-2560 (02) 542-2646</span>
                       </div>
                       <hr>
                   </div>
                   <div class="card-body user-info">
                       <div class="user-profile d-flex justify-content-center "><i class="fa fa-user-circle"></i></div>
                       {{--                                info student--}}
                       @foreach($studentNameData as $nameData)
                           <input type="hidden" name="fullname" value="{{ $nameData->first_name }} {{ $nameData->middle_name }} {{ $nameData->last_name }}">
                           <input type="hidden" name="student_id" value="{{ $nameData->student_id }}">
                       @endforeach
                       <div class="student-info">
                           <h1>Student Information</h1>
                           <hr>
                       </div>
                       <div class="user-info">
                           <h4><i class="fa fa-user-graduate"></i> Student</h4>
                           <div class="d-flex justify-content-between">
                               @foreach($studentNameData as $nameData )
                                   <h6>
                                       <span class="font-weight-bold">Student Name:</span>
                                       <span class="permanent" id="studentFullName">&nbsp;{{$nameData->first_name}} {{$nameData->middle_name}}, {{$nameData->last_name}}</span>
                                   </h6>
                                   <h6 class="mx-5">
                                       <span>Student I.D:</span>
                                       <span class="permanent">&nbsp;{{$nameData->student_id}}</span>
                                   </h6>
                               @endforeach
                           </div>
                           <div class="d-flex justify-content-between">
                               <h6>
                                           <span>Birth Date:
                                               <span class="permanent">
                                                   {{$s_bmonth}} {{ $s_bday }}, {{$s_byear}}
                                               </span>
                                           </span>
                               </h6>
                               <h6>
                                           <span class="mx-5">Email:
                                               <span class="forchange">
                                                   {{$s_email}}
                                                   <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="semail" class="semail" id="semail" placeholder="New Email" ></span>
                                                   @foreach($status as $pending)
                                                       @if($pending->type_of_column == 'change_in_email' && $pending->relationship == 'Student')
                                                           <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                                       @endif
                                                   @endforeach
                                               </span>
                                           </span>
                               </h6>
                           </div>
                           <div class="d-flex">
                               <h6 id="studentAddress">
                                   <span>House Address: </span>
                                   <span class="forchange">
                                               {{$s_hn}}
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="shn" class="shn" id="shn" placeholder="New House No." ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_house_number' && $pending->relationship == 'Student')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                                   <span class="forchange">
                                               {{$s_street}}
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="s-street" class="s-street" id="s-street" placeholder="New Street Address" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_street' && $pending->relationship == 'Student')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                                   <span class="forchange">
                                               {{$s_brgy}}
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="sbrgy" class="sbrgy" id="sbrgy" placeholder="New Barangay Area" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_barangay' && $pending->relationship == 'Student')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                                   <span class="forchange">
                                               {{$s_sub}}
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="sbuild" class="sbuild" id="sbuild" placeholder="New sbdv/bldg" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_subdivision_building' && $pending->relationship == 'Student')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                               </h6>
                           </div>
                           <div class="d-flex">
                               <h6>
                                   <span>City Address: </span>
                                   <span class="forchange">
                                               {{$s_district}}
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="sdistrict" class="sdistrict" id="sdistrict" placeholder="New City District" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_district' && $pending->relationship == 'Student')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                                   <span class="forchange">
                                               {{$s_city}}
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="scity" class="scity" id="scity" placeholder="New City" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_city' && $pending->relationship == 'Student')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                                   <span class="forchange">
                                               {{$s_prov}},
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="sprov" class="sprov" id="sprov" placeholder="New Province/City" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_province_municipality' && $pending->relationship == 'Student')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                                   <span class="forchange">
                                               {{$s_pcode}}
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="spcode" class="spcode" id="spcode" placeholder="New Postal code" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_postal_code' && $pending->relationship == 'Student')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                               </h6>
                           </div>
                           <div class="d-flex">
                               <h6>
                                   <span>Region:</span>
                                   <span class="forchange">
                                               {{$s_region}}
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="sregion" class="sregion" id="sregion" placeholder="New Region" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_region' && $pending->relationship == 'Student')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                               </h6>
                           </div>
                           <div>
                               <span>Parents Contact Number's</span>
                           </div>
                           <div class="d-flex">
                               <h6>
                                   <span class="mx-2">Mother's Contact No. :</span>
                                   <span class="forchange">
{{--                                                note detect if its mobile of telephone number then add 63+ if its mobile--}}
                                       {{--                                                urgent--}}
                                       @if(!empty($s_mcontact))
                                           {{$s_mcontact}}
                                       @else
                                           <span class="text-muted">There's no available contact</span>
                                       @endif
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="smnumber" class="smnumber" id="smnumber" placeholder="New contact no." ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_contact' && $pending->contact_relationship == 1 && $pending->relationship == 'Student')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                               </h6>
                           </div>
                           <div class="d-flex">
                               <h6>
                                   <span class="mx-2">Father's Contact No. :</span>
                                   <span class="forchange">
{{--                                                note detect if its mobile of telephone number then add 63+ if its mobile--}}
                                       {{--                                                urgent--}}
                                       @if(!empty($s_fcontact))
                                           {{$s_fcontact}}
                                       @else
                                           <span class="text-muted">There's no available contact</span>
                                       @endif
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="sfnumber" class="sfnumber" id="sfnumber" placeholder="New contact no." ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_contact' && $pending->contact_relationship == 2 && $pending->relationship == 'Student')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                               </h6>
                           </div>
                       </div>
                       <hr style="border: 1px dashed grey">
                       {{--                                mother part--}}
                       <div class="user-info">
                           <h4><i class="fa fa-female"></i> Mother</h4>
                           <div class="d-flex justify-content-between">
                               @foreach($motherData as $nameData )
                                   <h6>
                                       <span class="font-weight-bold">Mother's Name:</span>
                                       <span class="permanent" id="motherFullName">&nbsp;{{$nameData->first_name}} {{$nameData->middle_name}}, {{$nameData->last_name}}</span>
                                   </h6>
                                   <h6 class="mx-5">
                                       <span>Email:</span>
                                       <span class="forchange">&nbsp;{{$m_email}}</span>
                                       <span class="change"><i class="fa fa-arrow-alt-circle-right text-primary"></i> <input type="text" name="memail" class="memail" id="memail" placeholder="New Email" ></span>
                                       @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_in_email' && $pending->relationship == 'Mother')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                   </h6>
                               @endforeach
                           </div>
                           <div class="d-flex justify-content-between">
                               <h6>
                                           <span>Birth Date:
                                               <span class="permanent">
                                                   {{$m_bmonth}} {{ $m_bday }}, {{$m_byear}}
                                               </span>
                                           </span>
                               </h6>
                           </div>
                           <div class="d-flex">
                               <h6>
                                   <span>House Address: </span>
                                   <span class="forchange">
                                               {{$m_hn}}
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="mhn" class="mhn" id="mhn" placeholder="Enter new house #" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_house_number' && $pending->relationship == 'Mother')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                                   <span class="forchange">
                                               {{$m_street}}
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="m-street" class="m-street" id="m-street" placeholder="New Street Address" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_street' && $pending->relationship == 'Mother')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                                   <span class="forchange">
                                               {{$m_brgy}}
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="mbrgy" class="mbrgy" id="mbrgy" placeholder="New Barangay Area" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_barangay' && $pending->relationship == 'Mother')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                                   <span class="forchange">
                                               {{$m_sub}}
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="mbuild" class="mbuild" id="mbuild" placeholder="New sbdv/bldg" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_subdivision_building' && $pending->relationship == 'Mother')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                               </h6>
                           </div>
                           <div class="d-flex">
                               <h6>
                                   <span>City Address: </span>
                                   <span class="forchange">
                                               {{$m_district}}
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="mdistrict" class="mdistrict" id="mdistrict" placeholder="New City District" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_district' && $pending->relationship == 'Mother')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                                   <span class="forchange">
                                               {{$m_city}}
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="mcity" class="mcity" id="mcity" placeholder="New City" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_city' && $pending->relationship == 'Mother')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                                   <span class="forchange">
                                               {{$m_prov}},
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="mprov" class="mprov" id="mprov" placeholder="New Province/City" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_province_municipality' && $pending->relationship == 'Mother')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                                   <span class="forchange">
                                               {{$m_pcode}}
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="mpcode" class="mpcode" id="mpcode" placeholder="New Postal code" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_postal_code' && $pending->relationship == 'Mother')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                               </h6>
                           </div>
                           <div class="d-flex">
                               <h6>
                                   <span>Region:</span>
                                   <span class="forchange">
                                               {{$m_region}}
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="mregion" class="mregion" id="mregion" placeholder="New Region" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_region' && $pending->relationship == 'Mother')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                               </h6>
                           </div>
                       </div>
                       father part
                       <hr style="border: 1px dashed grey">
                       <div class="user-info">
                           <h4><i class="fa fa-male"></i> Father</h4>
                           <div class="d-flex justify-content-between">
                               @foreach($fatherData as $nameData )
                                   <h6>
                                       <span class="font-weight-bold">Father's Name:</span>
                                       <span class="permanent" id="fatherFullName">&nbsp;{{$nameData->first_name}} {{$nameData->middle_name}}, {{$nameData->last_name}}</span>
                                   </h6>
                                   <h6 class="mx-5">
                                       <span>Email:</span>
                                       <span class="forchange">&nbsp;{{$f_email}}</span>
                                       <span class="change"><i class="fa fa-arrow-alt-circle-right text-primary"></i> <input type="text" name="femail" class="femail" id="femail" placeholder="New Email" ></span>
                                       @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_in_email' && $pending->relationship == 'Father')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                   </h6>
                               @endforeach
                           </div>
                           <div class="d-flex justify-content-between">
                               <h6>
                                           <span>Birth Date:
                                               <span class="permanent">
                                                   {{$f_bmonth}} {{ $f_bday }}, {{$f_byear}}
                                               </span>
                                           </span>
                               </h6>
                           </div>
                           <div class="d-flex">
                               <h6>
                                   <span>House Address: </span>
                                   <span class="forchange">
                                               {{$f_hn}}
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="fhn" class="fhn" id="fhn" placeholder="Enter new house #" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_house_number' && $pending->relationship == 'Father')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                                   <span class="forchange">
                                               {{$f_street}}
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="f-street" class="f-street" id="f-street" placeholder="New Street Address" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_street' && $pending->relationship == 'Father')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                                   <span class="forchange">
                                               {{$f_brgy}}
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="fbrgy" class="fbrgy" id="fbrgy" placeholder="New Barangay Area" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_barangay' && $pending->relationship == 'Father')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                                   <span class="forchange">
                                               {{$f_sub}}
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="fbuild" class="fbuild" id="fbuild" placeholder="New sbdv/bldg" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_subdivision_building' && $pending->relationship == 'Father')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                               </h6>
                           </div>
                           <div class="d-flex">
                               <h6>
                                   <span>City Address: </span>
                                   <span class="forchange">
                                               {{$f_district}}
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="fdistrict" class="fdistrict" id="fdistrict" placeholder="New City District" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_district' && $pending->relationship == 'Father')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                                   <span class="forchange">
                                               {{$f_city}}
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="fcity" class="fcity" id="fcity" placeholder="New City" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_city' && $pending->relationship == 'Father')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                                   <span class="forchange">
                                               {{$f_prov}},
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="fprov" class="fprov" id="fprov" placeholder="New Province/City" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_province_municipality' && $pending->relationship == 'Father')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                                   <span class="forchange">
                                               {{$f_pcode}}
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="fpcode" class="fpcode" id="fpcode" placeholder="New Postal code" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_postal_code' && $pending->relationship == 'Father')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                               </h6>
                           </div>
                           <div class="d-flex">
                               <h6>
                                   <span>Region:</span>
                                   <span class="forchange">
                                               {{$f_region}}
                                               <span class="change"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="fregion" class="fregion" id="fregion" placeholder="New Region" ></span>
                                               @foreach($status as $pending)
                                           @if($pending->type_of_column == 'change_region' && $pending->relationship == 'Father')
                                               <span class="text-danger"><i class="fa fa-arrow-alt-circle-right"></i> {{ $pending->request_for_approval }}</span>
                                           @endif
                                       @endforeach
                                           </span>
                               </h6>
                           </div>
                       </div>
                       {{--                                sticky buttons--}}
                       <div class="sticky-buttons actionbtn mr-3">
                           <div class="row">
                               <button type="button" class="btn btn-primary shadow-lg" id="edit"><i class="fa fa-pencil-alt"></i></button>
                               <button type="submit" class="btn btn-success shadow-lg" id="submit"><i class="fa fa-arrow-alt-circle-down"></i></button>
                           </div>
                       </div>
                   </div>
               </div>
           </form>
       @else
           <form action="{{ route('store.newinfos') }}" method="POST">
               @csrf
               <div class="card info shadow my-3">
                   <div class="card-header-info">
                       <div class="d-flex ">
                           <img src="{{asset('img/XS_seal_update.png')}}" height="75" alt="Logo">
                           <h2 class="header-xs">Xavier School</h2>
                           <span class="header-xs-span">W.Conservation Avenue, Nuvali, Calamba Laguna</span>
                           <span class="header-xs-span-2">Telephone Numbers: (02) 664-0143 (049) 576-2560 (02) 542-2646</span>
                       </div>
                       <hr>
                   </div>
                   <div class="card-body user-info">
                       <div class="user-profile d-flex justify-content-center "><i class="fa fa-user-circle"></i></div>
                       {{--                                info student--}}
                       @foreach($studentNameData as $nameData)
                           <input type="hidden" name="fullname" value="{{ $nameData->first_name }} {{ $nameData->middle_name }} {{ $nameData->last_name }}">
                           <input type="hidden" name="student_id" value="{{ $nameData->student_id }}">
                       @endforeach


                       <div class="student-info">
                           <h1>Student Information</h1>
                           <hr>
                       </div>
                       <div class="user-info">
                           <h4><i class="fa fa-user-graduate"></i> Student</h4>
                           <div class="d-flex justify-content-between">
                               @foreach($studentNameData as $nameData )
                                   <h6>
                                       <span class="font-weight-bold">Student Name:</span>
                                       <span class="permanent">&nbsp;{{$nameData->first_name}} {{$nameData->middle_name}}, {{$nameData->last_name}}</span>
                                   </h6>
                                   <h6 class="mx-5">
                                       <span>Student I.D:</span>
                                       <span class="permanent">&nbsp;{{$nameData->student_id}}</span>
                                   </h6>
                               @endforeach
                           </div>
                           <div class="d-flex justify-content-between">
                                            <span>Age:
                                               <span class="permanent">
                                                   {{ $age }}
                                               </span>
                                           </span>
                           </div>
                           <div class="d-flex justify-content-between">
                               <h6>
                                           <span>Birth Date:
                                               <span class="permanent">
                                                   {{$s_bmonth}} {{ $s_bday }}, {{$s_byear}}
                                               </span>
                                           </span>
                                   <span>BirthPlace:
                                               <span class="permanent">
                                                    {{ $birthplace }}
                                               </span>
                                           </span>
                               </h6>
                           </div>
                           <div class="d-flex">
                               <h6>
                                   <div class="current-address-container">
                                   <span>Current Address:  </span>
                                       <span class="current-address" style="color: darkgreen;">{{ $student_curr_address }}</span>
                                       <div class="details">
                                           <span class="detail-process" style="color: red; font-size: 15px;"><small>Please input below the address you have submitted above. This is to ensure consistency on the data.</small></span>
                                       </div>
                                   </div>
                                   <span>House Address: </span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="shn" class="shn" id="shn" placeholder="Enter House No." ></span>
                                           </span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="s-street" class="s-street" id="s-street" placeholder="Enter Street Address" ></span>
                                           </span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="sbrgy" class="sbrgy" id="sbrgy" placeholder="Enter Brgy." ></span>
                                           </span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="sbuild" class="sbuild" id="sbuild" placeholder="Enter Bldg/Subd " ></span>
                                           </span>
                               </h6>
                           </div>
                           <div class="d-flex">
                               <h6>
                                   <span>City Address: </span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="sdistrict" class="sdistrict" id="sdistrict" placeholder="Enter City District"></span>
                                           </span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="scity" class="scity" id="scity" placeholder="Enter City"></span>
                                           </span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="sprov" class="sprov" id="sprov" placeholder="Enter Province/City"></span>
                                           </span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="spcode" class="spcode" id="spcode" placeholder="Enter Postal code"></span>
                                           </span>
                               </h6>
                           </div>
                           <div class="d-flex">
                               <h6>
                                   <span>Region:</span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="sregion" class="sregion" id="sregion" placeholder="Enter Region"></span>
                                           </span>
                               </h6>
                           </div>
                           <div>
                               <span>Contact Number:</span>
                           </div>
                           <div class="d-flex">
                               <h6>
                                           <span class="mx-2">Email:
                                               <span class="forchange">
                                                   @if(!empty($s_email))
                                                       {{$s_email}}
                                                   @else
                                                       <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="semail" class="semail" id="semail" placeholder="Enter Email"></span>
                                                   @endif
                                               </span>
                                           </span>
                               </h6>
                           </div>
                           <div class="d-flex">
                               <h6>
                                   <span class="mx-2">Landline Number :</span>
                                   <span class="forchange">
{{--                                                note detect if its mobile of telephone number then add 63+ if its mobile--}}
                                       {{--                                                urgent--}}
                                           <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="slandline" class="slandline" id="slandline" placeholder="Enter Landline no." ></span>
                                           </span>
                               </h6>
                           </div>
                           <div class="d-flex">
                               <h6>
                                   <span class="mx-2">Cellphone Number:</span>
                                   <span class="forchange">
{{--                                                note detect if its mobile of telephone number then add 63+ if its mobile--}}
                                       {{--                                                urgent--}}
                                           <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="scpnumber" class="scpnumber" id="scpnumber" placeholder="Enter cellphone no." ></span>
                                           </span>
                               </h6>
                           </div>
                       </div>
                       <hr style="border: 1px dashed grey">
                       {{--                                mother part--}}
                       <div class="user-info">
                           <h4><i class="fa fa-female"></i> Mother</h4>
                           <div class="d-flex justify-content-between">
                               @foreach($motherData as $nameData )
                                   <input type="hidden" name="fullname" value="{{ $nameData->last_name }}, {{ $nameData->first_name }} {{ $nameData->middle_name }}">
                                   <h6>
                                       <span class="font-weight-bold">Student Name:</span>
                                       <span class="permanent">&nbsp;{{$nameData->first_name}} {{$nameData->middle_name}}, {{$nameData->last_name}}</span>
                                   </h6>
                               @endforeach
                           </div>
                           <div class="d-flex justify-content-between">
                               <h6>
                                           <span>Birth Date:
                                               <span class="permanent">
                                                   {{$m_bmonth}} {{ $m_bday }}, {{$m_byear}}
                                               </span>
                                           </span>
                               </h6>
                           </div>
                           <div class="d-flex">
                               <h6>
                                   <div class="choicebox" style="margin-top: 5px; margin-bottom: 5px;">
                                       <span><input type="checkbox" name="motherAddressChoice" id="motherAddressChoice"></span>
                                       <span><label for="motherAddressChoice">Same Address as Student(if not, please leave unchecked)</label></span>
                                   </div>
                                   <span>House Address: </span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="mhn" class="mhn" id="mhn" placeholder="Enter House No." ></span>
                                           </span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="m-street" class="m-street" id="m-street" placeholder="Enter Street Address" ></span>
                                           </span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="mbrgy" class="mbrgy" id="mbrgy" placeholder="Enter Brgy." ></span>
                                           </span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="mbuild" class="mbuild" id="mbuild" placeholder="Enter Bldg/Subd " ></span>
                                           </span>
                               </h6>
                           </div>
                           <div class="d-flex">
                               <h6>
                                   <span>City Address: </span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="mdistrict" class="mdistrict" id="mdistrict" placeholder="Enter City District" ></span>
                                           </span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="mcity" class="mcity" id="mcity" placeholder="Enter City" ></span>
                                           </span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="mprov" class="mprov" id="mprov" placeholder="Enter Province/City" ></span>
                                           </span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="mpcode" class="mpcode" id="mpcode" placeholder="Enter Postal code" ></span>
                                           </span>
                               </h6>
                           </div>
                           <div class="d-flex">
                               <h6>
                                   <span>Region:</span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="mregion" class="mregion" id="mregion" placeholder="Enter Region" ></span>
                                           </span>
                               </h6>
                           </div>
                           <div>
                               <span>Contact Number:</span>
                           </div>
                           <div class="d-flex">
                               <h6>
                                           <span class="mx-2">Email:
                                               <span class="forchange">
                                                       <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="memail" class="memail" id="memail" placeholder="Enter Email"></span>
                                               </span>
                                           </span>
                               </h6>
                           </div>
                           <div class="d-flex">
                               <h6>
                                   <span class="mx-2">Landline Number :</span>
                                   <span class="forchange">
{{--                                                note detect if its mobile of telephone number then add 63+ if its mobile--}}
                                       {{--                                                urgent--}}
                                           <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="mlandline" class="mlandline" id="mlandline" placeholder="Enter Landline no." ></span>
                                           </span>
                               </h6>
                           </div>
                           <div class="d-flex">
                               <h6>
                                   <span class="mx-2">Cellphone Number:</span>
                                   <span class="forchange">
{{--                                                note detect if its mobile of telephone number then add 63+ if its mobile--}}
                                       {{--                                                urgent--}}
                                           <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="mcpnumber" class="mcpnumber" id="mcpnumber" placeholder="Enter cellphone no." ></span>
                                           </span>
                               </h6>
                           </div>
                       </div>
{{--                       father part--}}
                       <hr style="border: 1px dashed grey">
                       <div class="user-info">
                           <h4><i class="fa fa-male"></i> Father</h4>
                           <div class="d-flex justify-content-between">
                               @foreach($fatherData as $nameData )
                                   <h6>
                                       <span class="font-weight-bold">Father's Name:</span>
                                       <span class="permanent">&nbsp;{{$nameData->first_name}} {{$nameData->middle_name}}, {{$nameData->last_name}}</span>
                                   </h6>
                               @endforeach
                           </div>
                           <div class="d-flex justify-content-between">
                               <h6>
                                           <span>Birth Date:
                                               <span class="permanent">
                                                   {{$f_bmonth}} {{ $f_bday }}, {{$f_byear}}
                                               </span>
                                           </span>
                               </h6>
                           </div>
                           <div class="d-flex">
                               <h6>
                                   <div class="choicebox" style="margin-top: 5px; margin-bottom: 5px;">
                                       <span><input type="checkbox" name="fatherAddressChoice" id="fatherAddressChoice"></span>
                                       <span><label for="fatherAddressChoice">Same Address as Student(if not, please leave unchecked)</label></span>
                                   </div>
                                   <span>House Address: </span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="fhn" class="fhn" id="fhn" placeholder="Enter House No." ></span>
                                           </span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="f-street" class="f-street" id="f-street" placeholder="Enter Street Address" ></span>
                                           </span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="fbrgy" class="fbrgy" id="fbrgy" placeholder="Enter Brgy." ></span>
                                           </span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="fbuild" class="fbuild" id="fbuild" placeholder="Enter Bldg/Subd " ></span>
                                           </span>
                               </h6>
                           </div>
                           <div class="d-flex">
                               <h6>
                                   <span>City Address: </span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="fdistrict" class="fdistrict" id="fdistrict" placeholder="Enter City District" ></span>
                                           </span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="fcity" class="fcity" id="fcity" placeholder="Enter City" ></span>
                                           </span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="fprov" class="fprov" id="fprov" placeholder="Enter Province/City" ></span>
                                           </span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="fpcode" class="fpcode" id="fpcode" placeholder="Enter Postal code" ></span>
                                           </span>
                               </h6>
                           </div>
                           <div class="d-flex">
                               <h6>
                                   <span>Region:</span>
                                   <span class="forchange">
                                               <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="fregion" class="fregion" id="fregion" placeholder="Enter Region" ></span>
                                           </span>
                               </h6>
                           </div>
                           <div>
                               <span>Contact Number:</span>
                           </div>
                           <div class="d-flex">
                               <h6>
                                           <span class="mx-2">Email:
                                               <span class="forchange">
                                                       <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="memail" class="memail" id="memail" placeholder="Enter Email"></span>
                                               </span>
                                           </span>
                               </h6>
                           </div>
                           <div class="d-flex">
                               <h6>
                                   <span class="mx-2">Landline Number :</span>
                                   <span class="forchange">
{{--                                                note detect if its mobile of telephone number then add 63+ if its mobile--}}
                                       {{--                                                urgent--}}
                                           <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="flandline" class="flandline" id="flandline" placeholder="Enter Landline no." ></span>
                                           </span>
                               </h6>
                           </div>
                           <div class="d-flex">
                               <h6>
                                   <span class="mx-2">Cellphone Number:</span>
                                   <span class="forchange">
{{--                                                note detect if its mobile of telephone number then add 63+ if its mobile--}}
                                       {{--                                                urgent--}}
                                           <span class="newinput"><i class="fa fa-arrow-alt-circle-right"></i> <input type="text" name="fcpnumber" class="fcpnumber" id="fcpnumber" placeholder="Enter cellphone no." ></span>
                                           </span>
                               </h6>
                           </div>
                       </div>
                       <hr style="border: 1px dashed grey">
{{--                       <div class="user-info">--}}
{{--                           <span class="mx-2">In case of emergency, issuance of cheques and other notices please address to: </span>--}}
{{--                           <div class="d-flex">--}}
{{--                               <div class="form-check-inline ">--}}
{{--                                   <span class="emergency-radios"><input type="radio" name="motherDetails" id="motherDetails"></span>--}}
{{--                                   <span><label for="motherDetails">Mother</label></span>--}}
{{--                                   <span class="emergency-radios"><input type="radio" name="fatherDetails" id="fatherDetails"></span>--}}
{{--                                   <span class="emergency-radios"><label for="fatherDetails">Father</label></span>--}}
{{--                                   <span class="emergency-radios"><input type="radio" name="otherDetails" id="otherDetails"></span>--}}
{{--                                   <span><label for="otherDetails">Others</label></span>--}}
{{--                               </div>--}}
{{--                           </div>--}}
{{--                           <div class="d-flex">--}}

{{--                           </div>--}}
{{--                       </div>--}}
{{--                                                       sticky buttons--}}
                       <div class="sticky-buttons actionbtn mr-3">
                           <div class="row">
                               <button type="submit" class="btn btn-success shadow-lg" id="newsubmit"><i class="fa fa-arrow-alt-circle-down"></i></button>
                           </div>
                       </div>
                   </div>
               </div>
           </form>
      @endif
   </div>
@endsection
