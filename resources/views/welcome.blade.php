@extends('layouts.general')

@section('title', 'Welcome')

@section('content')
    <div id="home-main">
        <div class="topleft"></div>
        <div class="bottomRight"></div>
        <div class="hehe"></div>
        <nav class="navbar navbar-expand-sm navbar-dark container pt-5">
            <a href="/" class="navbar-brand d-flex"><img src="/img/seal.png" alt="logo" class="nav-seal">
                <div style="line-height: 1.05; color: black;">
                    <span><b>Xavier School Nuvali</b></span>
                    <br>
                    <span style="font-size: 0.73em;">Student Information</span>
                </div>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-content" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="nav-content">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a href="#footer" class="nav-link mr-2">Contact us</a>
                    </li>
                    <div class="dropdown-divider"></div>
                    <li class="nav-item">
                        <a href="/login" class="btn btn-warning"><b>Login</b></a>
                    </li>
                </ul>
            </div>
        </nav>
        <div id="main-content" class="container">
            <div class="d-flex landing-container">
                <div class="col-lg-7 left-info">
                    <h1>Student Information Portal</h1>
                    <p>An easy and convinient way to update your personal information On-The-Go</p>
                    <a class="btn btn-warning text-blue mt-4 p-2 pl-4 pr-4" href="#studentInformation" style="font-size: 1.5em;">Learn more</a>
                </div>
                <div class="col-lg-5 right-info">
                    <a href="https://www.freepik.com/search?dates=any&format=search&page=1&query=Login&sort=popular" target="_blank">
                        <img src=" {{ asset('royaltyfree/Portfolio-update.jpg') }}" class="landingimage shadow-lg" height="300" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection