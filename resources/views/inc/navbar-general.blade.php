{{-- Navbar for general web pages (login,verification) --}}
{{-- Note: add to includes + add content --}}
{{-- Format: logo + bold company name + system name --}}
{{-- Color: space gray / blue (from sidebar) --}}
{{-- Size: same as homepage / shorter --}}

<nav class="navbar sticky-top navbar-expand-lg navbar-light navbar-general">
    <div class="container">
        <a href="/" class="navbar-brand d-flex"><img src="/img/seal.png" alt="logo" class="nav-seal" style="height: 50px;">
            <div class="my-auto" style="line-height: 1.05; margin-left: 10px;">
                <span><b>Xavier School</b></span>
                <br>
                <span style="font-size: 0.73em;">Facility Reservation</span>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-general" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbar-general">
				<ul class="navbar-nav ml-auto mt-2 mt-lg-0">
					<li class="nav-item">
						<a href="/#footer" class="nav-link mr-2">Contact us</a>
					</li>
				</ul>
			</div>
        </a>
    </div>
</nav>