<form id="searchbar-wrapper">
	<div class="searchbar mr-auto">
		<input id="myInput" type="text" placeholder="Search" autocomplete="off">
		<div class="search-icon"><i class="fas fa-search"></i></div>
	</div>
</form>