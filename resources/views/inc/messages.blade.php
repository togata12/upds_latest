<div aria-live="polite" aria-atomic="true" class="toast-wrapper" id="toasterview" style="min-height: 200px;">
    <div class="toast-wrapper-sub">
		@if(count($errors))
			@foreach($errors->all() as $error)
				<div class="toast toast-warning" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="true" data-delay="10000">
		            <div class="toast-header">
		                <i class="fas fa-exclamation-triangle mr-1"></i>
		                <strong class="mr-auto">Notice</strong>
		                <small class="text-muted">Just now</small>
		                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
		                    <span aria-hidden="true">&times;</span>
		                </button>
		            </div>
		            <div class="toast-body">
		                {{ $error }}
		            </div>
		        </div>
			@endforeach
		@endif
		@if(session('success'))
			<div class="toast toast-success" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="true" data-delay="10000">
	            <div class="toast-header">
	                <i class="fas fa-check-circle mr-1"></i>
	                <strong class="mr-auto">Success</strong>
	                <small class="text-muted">Just now</small>
	                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="toast-body">
	                {{ session('success') }}
	            </div>
	        </div>
		@endif
		@if(session('error'))
			<div class="toast toast-danger" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="true" data-delay="10000">
	            <div class="toast-header">
	                <i class="fas fa-exclamation-triangle mr-1"></i>
	                <strong class="mr-auto">Alert</strong>
	                <small class="text-muted">Just now</small>
	                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="toast-body">
	                {{ session('error') }}
	            </div>
	        </div>
		@endif
	</div>
</div>

{{-- sidenote: notifications -> toast-info + fa-bell --}}