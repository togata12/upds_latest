@extends('layouts.general')

@section('title', 'Log in')

@section('content')
<div class="topRight"></div>
<div class="topLeft"></div>
<div class="bottomLeft"></div>
<div class="container-fluid">
    <div class="row">
        <div class="col col-md-7 welcome-container text-center">
            <a href="https://www.freepik.com/search?dates=any&format=search&page=1&query=Login&sort=popular">
                <img src="{{asset('/img/Login.jpg')}}"  alt="LoginPic">
            </a>
            <h1 class="welcome-header">Welcome</h1>
            <h5 class="welcome-header">Get ready for a whole new experience!</h5>
        </div>
        
        <div class="col col-md-5 login-form-container">
            
            @include('inc.navbar-general')
            <div class="container">
                <div class="login-header">
                    <div class="container-fluid">
                        <div class="row header-group">
                            <div class="col col-sm-12 title-container text-center">
                                <h4>Welcome Admin</h4>
                                <span class="font-weight-bold"><i class="fa fa-lock"></i> Log in to continue</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="login-forms">
                    <form method="POST" action="{{ route('login.admin') }}">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control @error('student_id') is-invalid @enderror"  name="admin_id" id="student_id" placeholder="Admin_ID" required>
                    
                            @error('admin_id')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    
                        <div class="form-group">
                            <input type="password" class="form-control @error('password') is-invalid @enderror"  id="password" name="password" placeholder="Password" >
                            @error('password')
                            <strong>{{ $message }}</strong>
                            @enderror
                        </div>
                    
                        <button type="submit" class="btn btn-primary w-100 submit-btn">{{__('Login')}}</button>
                    </form>
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection
