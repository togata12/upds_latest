<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title') - Student Information System</title>
         {{-- FAVICON --}}
        <link rel="apple-touch-icon" href="{{ asset('img/golden_stallion.PNG') }}">
        <link rel="shortcut icon" href="{{ asset('img/golden_stallion.PNG') }}">
        <!-- Scripts -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="{{asset('js/menutoggle.js')}}" defer></script>
        <script src="{{asset('/js/jquery-modal.js')}}" defer></script>
        <script src="{{ asset('/js/getSameInput.js') }}" defer></script>
        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/simple-sidebar.css') }}">
        <link rel="stylesheet" href="{{ asset('css/dashboard.css')}}">
        {{-- Font styles --}}
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway:500&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">

    </head>
    <body>
        <div class="d-flex" id="wrapper">
            <!-- Sidebar -->
            <div class="sidebar border-light" id="sidebar-wrapper">
                <div class="sidebar-heading">
                    <div class="navbar-brand ml-2">
                        <img src="{{asset('img/XS_seal_update.png')}}" alt="Logo">
                        <div class="text-left navbar-text text-heading ml-2">
                            <h3 style="font-size: 15px; margin-right: 10px">Xavier School<br><span style="font-size: 10px; margin-top: -10px">Student Information</span></h3>
                        </div>
                    </div>
                </div>
                {{-- Side bar link --}}
                <div class="list-group">
                    <a href="#" class="list-group-item list"><i class="fas fa-file-alt" style="margin-right: 10px"></i>Information Board</a>
                </div>
            </div>
            <div id="page-content-wrapper">
                <div class="card card-pos shadow-sm">
                    <div class="card-body">
                        <a href="#" class="text-dark" id="menu-toggle"><i class="fas fa-bars"></i></a>
                        <a class="float-right dropdown-toggle text-dark" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user-circle"></i> {{ Auth::user()->first_name }}</a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item text-danger" href="{{ route('logout')}} " onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-lock"></i> Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    @yield('existinguser')
                    @include('inc.messages')
                </div>
            </div>
        </div>
         {{-- SCRIPTS --}}
         <script src="{{ asset('js/script.js') }}"></script>
    </body>
</html>
