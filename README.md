## Clone repository
git clone https://togata12@bitbucket.org/togata12/upds_latest.git

---

## install node dependencies

open cmd, navigate to root folder then type
npm install
wait to finish

---

## install laravel dependencies

open cmd, navigate to root folder then type
composer install
wait to finish

---

## copy db from repo to phpMyadmin

go to phpMyAdmin
Select import then choose "updating_system.vs1"

wait to load

---

## User Credentials

Username: 2018-0003
Password: nani123

##Admin Credentials

at the searchbar at top of browser append to link /admin/login

then type credentials

Username: xsadmin-01
Password: masteradmin001

Feel free to explore(db not yet final btw)

---

## FOR NEW UPDATES AND UPON PULLING UPDATES

if there are updates on the node_modules please run "npm install" again to update dependencies

---

to update laravel dependencies(the ones at the vendor folder) please run at cmd "composer update"
